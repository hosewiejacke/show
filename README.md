# show

show gathers information from Cisco switches and displays it as a table.

### Example

	$ show mac host int vlan
	9c:93:4e:xx:xx:xx    cat3560      Gi0/21       108
	38:c9:86:xx:xx:xx    cat3560      Gi0/19       14
	00:23:ae:xx:xx:xx    cat4506-1    Gi4/14       17
	9c:93:4e:xx:xx:xx    cat4506-2    Gi3/42       108

### Usage

	$ show (interface | mac) [column[:width] ...]

The column names can be abbreviated as long as they are unambiguous:

* `description`  
  show interface description

* `hostname`  
  show switch hostname

* `interface`  
  show interface name, e. g. Gi0/1

* `mac`  
  show MAC address

* `status`  
  show state of the interface

* `vlan`  
  show VLAN ID

### Configuration

`show` reads `/etc/show.conf` to get the list of switches and their community strings.

	$ cat /etc/show.conf
	community 12345			# default community string

	[cat3560]
		community 123456	# cat3560 has a special community string
	[cat4506-1]
	[cat4506-2]

### Notes

* depends on Net::SNMP
	- Debian: `libnet-snmp-perl`
	- OpenBSD: `p5-Net-SNMP`

* parallel SNMP queries lowered the overall performance
* if you have benchmarks how this tool performs compared to SSH when using Rex or Ansible please let me know
