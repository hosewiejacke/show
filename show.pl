#!/usr/bin/env perl

use warnings;
use strict;
use feature 'state';
use Net::SNMP;

# prototypes
sub show($);
sub snmpwalk($$$$);
sub snmpget($$$$);
sub vlans($);
sub usage();

# global variables
my $config = "/etc/show.conf";

# Cisco OIDs
my $OID_vtpVlanState =			'1.3.6.1.4.1.9.9.46.1.3.1.1.2';
my $OID_dot1dTpFdbAddress =		'1.3.6.1.2.1.17.4.3.1.1';
my $OID_dot1dTpFdbPort =		'1.3.6.1.2.1.17.4.3.1.2';
my $OID_ifName =				'1.3.6.1.2.1.31.1.1.1.1';
my $OID_ifAlias =				'1.3.6.1.2.1.31.1.1.1.18';
my $OID_dot1dBasePortIfIndex =	'1.3.6.1.2.1.17.1.4.1.2';
my $OID_vmVlan =				'1.3.6.1.4.1.9.9.68.1.2.2.1.2';
my $OID_ipNetToPhysicalTable =	'1.3.6.1.2.1.4.35'; # . ifIndex
my $OID_ifIndex =				'1.3.6.1.2.1.2.2.1.1';
my $OID_ifAdminStatus =			'1.3.6.1.2.1.2.2.1.7';

# implemented columns
my %column = (
		   'interface'		=> [\&interface, # sub
			   					'interface name, e. g. Gi0/1', # help
								9, # width
								1], # primary?
		   'mac'            => [\&mac,
		   						'MAC address',
								17,
								1],
		   'description'	=> [\&description,
								'interface description',
								11,
							    0],
		   'vlan'			=> [\&vlan,
								'VLAN ID',
								3,
							    0,],
			'hostname'		=> [\&hostname,
								'switch hostname',
								17,
							    0],
			#'index'			=> [\&index,
			#					'interface index (for debugging purposes)',
			#					3,
			#					0],
			#'room'			=> [\&room,
			#					'room (works only for registered ports)',
			#					6,
			#				    0],
			'status'		=> [\&status,
								'state of the interface',
								7,
							    0],
		);

# user must specify 1+ colums. A table without columns doesn't make sense
if($#ARGV < 0){
	usage();
	exit -1;
}

# make sure we've got valid and unambiguous columns
my @columns = ();
foreach my $col (@ARGV){

    my $width = $1 if $col =~ s/:(\d+)$//; # cut off optional width
    my @matches = grep(/^$col/, keys %column);

	die "invalid column name: $col\n" if @matches == 0;
	die "ambiguous column name: $col\n" if @matches > 1;

	push @columns, $matches[0]; # push column's unabbreviated name
    $column{$matches[0]}[2] = $width if defined $width; # override column width
}

# make sure first column is an index column
$column{$columns[0]}[3] == 1 or
    die "\"$columns[0]\" can't be used as first column";

# get config
my ($conf, $err) = &conf($config);
!defined $err or die "Error in $config: $err";

my @switches = keys %$conf;

# need at least 1 switch defined in /etc/show.conf
@switches != 0 or
die "You didn't specify any switches in $config. Example:

    community secret1   # specify default community string
    os ios              # specify default operating system

    [cat2950-1]
    community secret2   # override default community string for this switch

    [cat3560-1]         # use defaults
    [cat3560-2]
";

foreach(@switches){
	show({
		switch=>$_,
		conf=>$conf->{$_},
		columns=>\@columns,
	});
}

# main workhorse
sub show($)#{{{
{ # required parameters: switch, vlans, community

	my ($argv) = @_;

	my @columns; # array of hashes, ifIndex as key
	my @ifIndices; # ifIndex, not (0..n)
	my @width;   # width of columns

	return -1 if not exists $argv->{switch};

	# open snmp session to given switch
	my $vlan = 1;
	my ($session, $error) = Net::SNMP->session(
							-hostname => $argv->{switch},
							-version => 'snmpv2c',
							-community => "$argv->{conf}->{community}$vlan"
						);
	die "$error" if not defined $session;

	# ifAliases of 1st row are used as keys for all other hash iterations
	my $title = shift @{$argv->{columns}}; # title of first column
	my $base_column = $column{$title}[0]->($session);
	@ifIndices = keys %{$base_column};
	$base_column->{title} = $title;

	# iterate over wanted columns and call appropriate subs
	foreach(@{$argv->{columns}}){
		my $col = $column{$_}[0]->($session, $argv->{conf});
		$col->{title} = $_; # so we know later what column this is
		push @columns, $col;
	}

	# inject base column and restore argv
	unshift @columns, $base_column;
	unshift @{$argv->{columns}}, $title;

	# print values
	foreach my $ifIndex (@ifIndices) {
		foreach (@columns) {
			my $width = $column{$_->{title}}[2]; # width of the column
			printf "%-$width.${width}s    ", $_->{$ifIndex} // "";
		}
		print "\n"; # row ready
	}
	$session->close();
}#}}}

# various subs
sub vlans($)#{{{
{ # return available vlans for given session
	my ($session) = @_;
	my @vlans;
	
	my $snmp_result = $session->get_table(-baseoid => $OID_vtpVlanState);
	foreach(keys %$snmp_result){
		#print "vlans(): $_ -- $snmp_result->{$_}\n";
		if($$snmp_result{$_} eq '1'){ # if vlan operational
			s/.*\.//g;
			push @vlans, $_;
		}
	}
	return @vlans;
}#}}}
sub usage() #{{{
{
	print "usage: $0 (",
        join (" | ", grep ({$column{$_}[3] == 1} sort keys %column)),
        ") [<list of columns>]\n",
        "\ncolumns:\n";

	foreach(sort keys %column){
		printf "    %-12.12s    %s\n", $_, $column{$_}[1];
	}
} #}}}
sub conf($)#{{{
{ # return configuration. (all switches = keys %conf)

	my ($config) = @_;	# path
	
	# return
	my %conf;			# hashref of hashes
	my $err = undef;	# indicate error to caller

	# default config
	my %default = (
		os			=> "ios",
		community	=> "geheim",
        portmap	    => "./portmap.txt"
	);

	if(-e $config) {
		open(my $CONFIG, '<', $config) or die $!;

		my $host = undef; # undef means global section

		while(<$CONFIG>){
			chomp;
			s/#.*//; # drop everything after #
			next if /^\s*$/; # skip empty line

			if(/^\s*\[\s*(\S+)\s*\]\s*$/){ # e. g. [cat2950-42]
				$host = $1;

				# init new section with default values
				@{$conf{$host}}{keys %default} = values %default;
			}
			elsif(/^\s*(\S+)\s+(.*)\s*$/){ # e. g. 'community geheim'

				# allow only specified keys
				if(!exists $default{$1}){
					$err = sprintf "unknown key '%s' near '%s'", $1, $_;
					last;
				}

				# insert into global or per-host section
				if(defined $host) { $conf{$host}{$1} = $2; }
				else {				$default{$1} = 	   $2; }
			}
			else {
				$err = sprintf "error in %s near %s", $config, $_;
				last;
			}
		}
	}
	return \%conf, $err;
}#}}}

# column subs
sub interface($)#{{{
{ # interface name, e.g. Gi0/1

	my ($session) = @_;
	my $result = $session->get_table(-baseoid => $OID_ifName);
	my %interface = (); # ifIndex as key, ifName as value

	foreach(keys %$result){
		#print "$1 : $result->{$_} \n"

		/(\d+)$/;
		$interface{$1} = $result->{$_}; 
	}
	print 
	return \%interface;
}#}}}
sub mac($)#{{{
{ # mac address
	my ($session) = @_;

	my %mac = (); # ifIndex as key, mac as value
	my %ifIndex = (); # bridgeport as key, ifIndex as value
	my $snmp_result;

	foreach my $vlan (vlans($session)){
		my $hostname = $session->hostname();

		my ($vlan_session, $error) = Net::SNMP->session(
			-hostname => $hostname,
			-version => 'snmpv2c',
			-community => "$conf->{$hostname}{community}$vlan" # (*)
		);
		die "$error" if not defined $vlan_session;

		# (*)
		# this is ugly because we access $conf from main. Did it anyway
		# since all column functions get only a ready-to-use session
		# and don't know about the community string

		# there're no ifIndex->MAC entries. Fetch ifIndex->bridgeport first
		$snmp_result = $vlan_session->get_table(-baseoid => 
			$OID_dot1dBasePortIfIndex); # e.g. mib-2.17.1.4.1.2.163 91

		foreach (keys %$snmp_result){
			/(\d+)$/; # brigeport is the last digit from key
			$ifIndex{$1} = $snmp_result->{$_};
		}

		# now fetch mac->bridgeport
		$snmp_result = $vlan_session->get_table(-baseoid =>
			$OID_dot1dTpFdbPort);# e.g. mib-2.17.4.3.1.2.0.37.100.162.82.253 163

		foreach (keys %$snmp_result){
			my $bridgeport = $snmp_result->{$_}; # bridgeport
			next if $bridgeport == 0;

			/(\d+)\.(\d+)\.(\d+)\.(\d+)\.(\d+)\.(\d+)$/;
			my $m = sprintf "%02x:%02x:%02x:%02x:%02x:%02x",
						$1, $2, $3, $4, $5, $6;
			$mac{$ifIndex{$bridgeport}} = $m;
			#print "$ifIndex{$snmp_result->{$_}} = $m\n";
		}
	}

	return \%mac;
}#}}}
sub description($)#{{{
{ # interface description

	my ($session) = @_;
	my $result = $session->get_table(-baseoid => $OID_ifAlias);
	my %description = (); # ifIndex as key, ifName as value

	foreach(keys %$result){
		#print "$1 : $result->{$_} \n"

		/(\d+)$/;
		$description{$1} = $result->{$_}; 
	}
	return \%description;
}#}}}
sub vlan($)#{{{
{
	my ($session) = @_;
	my $result = $session->get_table(-baseoid => $OID_vmVlan);
	my %vlan = (); # ifIndex as key, vlan as value

	foreach(keys %$result){
		/(\d+)$/;
		$vlan{$1} = $result->{$_}; 
	}
	return \%vlan;
}#}}}
sub hostname($)#{{{
{ # switch

	my ($session) = @_;
	my %switch = ();
	my $hostname = $session->hostname();

	# map all ifIndices to the same switch (not really efficient...)
	my $result = $session->get_table(-baseoid => $OID_ifAlias);
	foreach(keys %$result){
		/(\d+)$/;
		$switch{$1} = $hostname; 
	}
	return \%switch;
}#}}}
sub index($)#{{{
{ # interface index (for debugging purposes)

	my ($session) = @_;
	my $result = $session->get_table(-baseoid => $OID_ifIndex);
	my %index = (); # ifIndex as key, ifName as value

	foreach(keys %$result){
		/(\d+)$/;
		$index{$1} = $result->{$_}; 
	}
	return \%index;
}#}}}
#sub room($)#{{{
#{ # interface room
#
#	my ($session, $conf) = @_;
#	my $result = $session->get_table(-baseoid => $OID_ifAlias);
#	my %room = (); # ifIndex as key, room as value
#
#    state %portmap; # hash of hashes
#    
#    if (!exists $portmap{$conf->{portmap}}) {
#        print "called sub\n";
#
#        my %pm; # portmap read from single file
#        
#        open $PORTMAP, '<', $conf->{portmap} or die $!;
#        while (<$PORTMAP>) {
#            chomp;
#			s/#.*//; # drop everything after #
#			next if /^\s*$/; # skip empty line
#
#			if (/^\s*(\S+)\s+(.*)\s*$/) {
#                $pm{$1} = $2;
#            }
#            else {
#				sprintf "error in %s near %s", $conf->{portmap}, $_;
#				last;
#			}
#        }
#        %portmap{$conf->{portmap}
#            
#        close $PORTMAP;
#    }
#
#    my %map;
#    
#	foreach(keys %$result){
#		#print "$1 : $result->{$_} \n"
#
#		/(\d+)$/;
#		$room{$1} = $map{$result->{$_}};
#	}
#	return \%room;
#}#}}}
sub status($)#{{{
{
	my ($session) = @_;
	my $result = $session->get_table(-baseoid => $OID_ifAdminStatus);
	my %status = (); # ifIndex as key, status as value
	my @states = (undef, 'up', 'down', 'tesing');

	foreach(keys %$result){
		/(\d+)$/;
		$status{$1} = $states[$result->{$_}]; 
	}
	return \%status;
}#}}}
